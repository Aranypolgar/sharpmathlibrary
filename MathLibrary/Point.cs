﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MathLibrary
{
    public class Point
    {
        public int X { get; set; }
        public int Y { get; set; }

        public Point()
        {
            this.X = 0;
            this.Y = 0;
        }

        public Point(int x, int y)
        {
            this.X = x;
            this.Y = y;
        }

        //GET: Two Point Addition
        public static Point operator +(Point point1, Point point2)
        {
            return new Point((point1.X + point2.X),
                (point1.Y + point2.X));
        }

        //GET: Two Point Subtraction
        public static Point operator -(Point point1, Point point2)
        {
            return new Point((point1.X - point2.X),
                (point1.Y - point2.X));
        }

        //GET: Two Point Multiplication
        public static Point operator *(Point point1, Point point2)
        {
            return new Point((point1.X * point2.X),
                (point1.Y * point2.X));
        }

        //GET: Two Point Division
        public static Point operator /(Point point1, Point point2)
        {
            if (point2.X == 0 || point2.Y == 0) return null;

            return new Point((point1.X / point2.X),
                (point1.Y / point2.X));
        }
    }
}
