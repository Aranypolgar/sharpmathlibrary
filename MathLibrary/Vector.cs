﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MathLibrary
{
    public class Vector
    {
        int[] _vector;

        public Vector(int size)
        {
            this._vector = new int[size];
        }

        public int this[int index]
        {
            get => this._vector[index];
            set => this._vector[index] = value;
        }

        //GET: Return vector length
        public int Length => this._vector.Length;

        private int sum;

        public int Sum
        {
            get
            {
                this.sum = 0;

                for(int i = 0; i < this._vector.Length; i++)
                {
                    this.sum += this._vector[i]; 
                }

                return this.sum;
            }
        }

        //GET: Two Vector Addition
        public static Vector operator +(Vector vector1, Vector vector2)
        {
            int v1_size = vector1.Length;
            int v2_size = vector2.Length;
            int Greater = v1_size >= v2_size ? v1_size : v2_size;
            int Smallest = v1_size <= v2_size ? v1_size : v2_size;

            Vector result = new Vector(Greater); 

            for(int i = 0; i < Greater; i++)
            {
                if(i < Smallest)
                {
                    result[i] = vector1[i] + vector2[i];
                }
                else
                {
                    if(vector1.Length > vector2.Length)
                    {
                        result[i] = vector1[i];
                    }
                    else
                    {
                        result[i] = vector2[i];
                    }
                }
            }

            return result;
        }

        //GET: Two Vector Subtraction
        public static Vector operator -(Vector vector1, Vector vector2)
        {
            int v1_size = vector1.Length;
            int v2_size = vector2.Length;
            int GreaterSize = v1_size >= v2_size ? v1_size : v2_size;

            Vector result = new Vector(GreaterSize);

            for (int i = 0; i < GreaterSize; i++)
            {
                result[i] = vector1[i] - vector2[i];
            }

            return result;
        }

        //GET: Two Vector Multiplication
        public static Vector operator *(Vector vector1, Vector vector2)
        {
            int v1_size = vector1.Length;
            int v2_size = vector2.Length;
            int GreaterSize = v1_size >= v2_size ? v1_size : v2_size;

            Vector result = new Vector(GreaterSize);

            for (int i = 0; i < GreaterSize; i++)
            {
                result[i] = vector1[i] * vector2[i];
            }

            return result;
        }

        //GET: Two Vector Division
        public static Vector operator /(Vector vector1, Vector vector2)
        {
            int v1_size = vector1.Length;
            int v2_size = vector2.Length;
            int GreaterSize = v1_size >= v2_size ? v1_size : v2_size;

            Vector result = new Vector(GreaterSize);

            for (int i = 0; i < GreaterSize; i++)
            {
                if (vector2[i] != 0)
                {
                    result[i] = vector1[i] / vector2[i];
                }
            }

            return result;
        }

    }
}
