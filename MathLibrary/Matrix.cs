﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MathLibrary
{
    public class Matrix
    {
        int[,] _matrix;

        public Matrix(int row, int col)
        {
            this._matrix = new int[row, col];
        }

        //GET: Return value by index
        public int this[int index_row, int index_col]
        {
            get => _matrix[index_row, index_col];
            set => _matrix[index_row, index_col] = value;
        }

        //GET : Return matrix row
        public int RowLength => this._matrix.GetLength(0);

        //GET : Return matrix col
        public int ColLength => this._matrix.GetLength(1);

        //GET: Two Matrix Addition
        public static Matrix operator +(Matrix matrix1, Matrix matrix2)
        {
            //Check the matrix sizes
            if(matrix1.RowLength != matrix2.RowLength 
                || matrix1.ColLength != matrix2.ColLength)
            {
                return null;
            }

            int row = matrix1.RowLength;
            int col = matrix1.ColLength;
            var result = new Matrix(row, col);

            //Addition and save result
            for(int i = 0; i < row; i++)
            {
                for(int j = 0; j< col; j++)
                {
                    result[i, j] = matrix1[i, j] + matrix2[i, j];
                }
            }

            return result;
        }

        //GET: Two Matrix Subtraction
        public static Matrix operator -(Matrix matrix1, Matrix matrix2)
        {
            //Check the matrix sizes
            if (matrix1.RowLength != matrix2.RowLength
                || matrix1.ColLength != matrix2.ColLength)
            {
                return null;
            }

            int row = matrix1.RowLength;
            int col = matrix1.ColLength;
            var result = new Matrix(row, col);

            //Addition and save result
            for (int i = 0; i < row; i++)
            {
                for (int j = 0; j < col; j++)
                {
                    result[i, j] = matrix1[i, j] - matrix2[i, j];
                }
            }

            return result;
        }

        //GET: Two Matrix Multiplication
        public static Matrix operator *(Matrix matrix1, Matrix matrix2)
        {
            //Check the matrix sizes
            if (matrix1.RowLength != matrix2.RowLength
                || matrix1.ColLength != matrix2.ColLength)
            {
                return null;
            }

            int row = matrix1.RowLength;
            int col = matrix1.ColLength;
            var result = new Matrix(row, col);

            //Addition and save result
            for (int i = 0; i < row; i++)
            {
                for (int j = 0; j < col; j++)
                {
                    result[i, j] = matrix1[i, j] * matrix2[i, j];
                }
            }

            return result;
        }

        //GET: Two Matrix Division
        public static Matrix operator /(Matrix matrix1, Matrix matrix2)
        {
            //Check the matrix sizes
            if (matrix1.RowLength != matrix2.RowLength
                || matrix1.ColLength != matrix2.ColLength)
            {
                return null;
            }

            int row = matrix1.RowLength;
            int col = matrix1.ColLength;
            var result = new Matrix(row, col);

            //Addition and save result
            for (int i = 0; i < row; i++)
            {
                for (int j = 0; j < col; j++)
                {
                    if (matrix2[i, j] != 0)
                    {
                        result[i, j] = matrix1[i, j] / matrix2[i, j];
                    }
                }
            }

            return result;
        }

        //GET Matrix Value
        private int _value;
        
        public int Value
        {
            get {

                this._value = 0;

                for (int i = 0; i < this._matrix.GetLength(0); i++)
                {
                    for (int j = 0; j < this._matrix.GetLength(1); j++)
                    {
                        this._value += this._matrix[i, j];
                    }
                }

                return this._value;
            }
 
        }

    }
}
