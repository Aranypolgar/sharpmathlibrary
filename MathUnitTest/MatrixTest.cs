﻿using System;
using MathLibrary;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace MathUnitTest
{
    [TestClass]
    public class MatrixTest
    {
        [TestMethod]
        public void MatrixAdding()
        {
            Matrix matrix1 = new Matrix(1, 1);
            Matrix matrix2 = new Matrix(1, 1);

            matrix1[0, 0] = 2;
            matrix2[0, 0] = 3;
            int ExpectedResult = 5;
            int value = (matrix1 + matrix2)[0,0];

            Assert.AreEqual(ExpectedResult, value);
        }

        [TestMethod]
        public void MatrixValue()
        {
            Matrix matrix = new Matrix(2, 2);

            for(int i = 0; i< matrix.RowLength; i++)
            {
                for (int j = 0; j < matrix.ColLength; j++)
                {
                    matrix[i, j] = 2;
                }

            }
            
            int Expected = 8;
            int Value = matrix.Value;

            Assert.AreEqual(Expected, Value);
        }
    }
}
